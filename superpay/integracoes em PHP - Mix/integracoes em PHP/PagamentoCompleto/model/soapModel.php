<?php
include "lib/webservices.php";
class soapModel{
	var $USUARIO_SUPERPAY = 'ERNET'; //Informado pelo suporte
	var $SENHA_SUPERPAY = 'ERNET'; //Informado pelo suporte
	
	function consultaTransacaoEspecifica($dados_envio){
		$parametros = array('consultaTransacaoWS'=>$dados_envio, 'usuario'=>$this->USUARIO_SUPERPAY, 'senha'=>$this->SENHA_SUPERPAY);
		$funcao_chamada = 'consultaTransacaoEspecifica';
		$retorno = callWebServices($parametros,$funcao_chamada,"http://homologacao.superpay.com.br/superpay/servicosPagamentoCompletoWS.Services?wsdl");
		return $retorno;		
	}

	function pagamentoCompleto($dados_envio){
		$parametros = array('transacao'=>$dados_envio, 'usuario'=>$this->USUARIO_SUPERPAY, 'senha'=>$this->SENHA_SUPERPAY);
		$funcao_chamada = 'pagamentoTransacaoCompleta';
		$retorno = callWebServices($parametros,$funcao_chamada,"http://homologacao.superpay.com.br/superpay/servicosPagamentoCompletoWS.Services?wsdl");
		return $retorno;
	}
	
	function pagamentoCompletoMaisCartao($dados_envio){
		$parametros = array('transacao'=>$dados_envio, 'usuario'=>$this->USUARIO_SUPERPAY, 'senha'=>$this->SENHA_SUPERPAY);
		$funcao_chamada = 'pagamentoTransacaoCompletaMaisCartoesCredito';
		$retorno = callWebServices($parametros,$funcao_chamada,"http://homologacao.superpay.com.br/superpay/servicosPagamentoCompletoWS.Services?wsdl");
		return $retorno;
	}
	
	function operacaoTransacao($dados_envio){
		$parametros = array('operacao'=>$dados_envio, 'usuario'=>$this->USUARIO_SUPERPAY, 'senha'=>$this->SENHA_SUPERPAY);
		$funcao_chamada = 'operacaoTransacao';
		$retorno = callWebServices($parametros,$funcao_chamada,"http://homologacao.superpay.com.br/superpay/servicosPagamentoCompletoWS.Services?wsdl");
		return $retorno;
	}
	
	function criarRecorrencia($dados_envio){		
		$parametros = array('recorrenciaWS'=>$dados_envio, 'usuario'=>array("usuario"=>$this->USUARIO_SUPERPAY, 'senha'=>$this->SENHA_SUPERPAY));
		$funcao_chamada = 'cadastrarRecorrenciaWS';
		$retorno = callWebServices($parametros,$funcao_chamada,"http://homologacao.superpay.com.br/superpay/servicosRecorrenciaWS.Services?wsdl");
		return $retorno;
	}
	
	function consultarRecorrencia($dados_envio){		
		$parametros = array('recorrenciaConsultaWS'=>$dados_envio, 'usuario'=>array("usuario"=>$this->USUARIO_SUPERPAY, 'senha'=>$this->SENHA_SUPERPAY));
		$funcao_chamada = 'consultaTransacaoRecorrenciaWS';
		$retorno = callWebServices($parametros,$funcao_chamada,"http://homologacao.superpay.com.br/superpay/servicosRecorrenciaWS.Services?wsdl");
		return $retorno;
	}
	
	function cancelarRecorrencia($dados_envio){
		$parametros = array('recorrenciaCancelarWS'=>$dados_envio, 'usuario'=>array("usuario"=>$this->USUARIO_SUPERPAY, 'senha'=>$this->SENHA_SUPERPAY));
		$funcao_chamada = 'cancelarRecorrenciaWS';
		$retorno = callWebServices($parametros,$funcao_chamada,"http://homologacao.superpay.com.br/superpay/servicosRecorrenciaWS.Services?wsdl");
		return $retorno;
	}
	
	
	
}
?>
