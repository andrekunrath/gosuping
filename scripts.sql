ALTER TABLE `Usuario`
MODIFY COLUMN `Senha`  varchar(35) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL AFTER `Email`;

ALTER TABLE `Pedido`
ADD COLUMN `TransacaoID`  int(11) NULL AFTER `StatusTransacaoID`;

ALTER TABLE `Usuario`
MODIFY COLUMN `CPF`  varchar(11) NOT NULL AFTER `Senha`;

