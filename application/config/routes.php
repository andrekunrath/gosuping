<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'app/index';
$route['404_override'] = '';

/*
 * Rotas do app mobile
 */

$route['app'] = 'app/index';

$route['app/login'] = 'app/login/index';
$route['app/login/recuperar-senha'] = 'app/login/recuperarsenha';
$route['app/login/deslogar'] = 'app/login/deslogar';

$route['app/cadastro'] = 'app/cadastro/index';


//$route['app/pedido-adquira'] = 'app/pedido/adquira'; // passo 1 - iniciar
$route['app/pedido'] = 'app/pedido/index'; // passo 2 - pagamento
//$route['app/pedido-sucesso'] = 'app/pedido/sucesso'; // passo 3 - sucesso
//$route['app/pedido-prancha'] = 'app/pedido/prancha'; // passo 4 - escolha da prancha
//$route['app/pedido-fim'] = 'app/pedido/fim'; // passo 5 - fim

$route['app/meus-pedidos'] = 'app/pedido/historico'; // meus pedidos

$route['app/termos'] = 'app/pedido/termos';

/* End of file routes.php */
/* Location: ./application/config/routes.php */

