<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
|--------------------------------------------------------------------------
| Constantes pertinentes ao projeto GOSUPING
|--------------------------------------------------------------------------
|
*/

/*
 * Constantes gerais
 */
define('FORMATO_DATA', 'Y-m-d H:i:s');

/*
 * Referente a e-mails
 */
define('EMAIL_ESQUECI_SENHA_FROM', 			'mozarthess@gmail.com');
define('EMAIL_ESQUECI_SENHA_FROM_NOME', 	'mozart');
define('EMAIL_ESQUECI_SENHA_ASSUNTO', 		'GOSUPING - Recuperação de senha');

/*
 * ATENÇÃO!! Se esta constante estiver true, o fluxo de pedido será SEMPRE aprovado!
 */
define('TESTAR_FLUXO_PEDIDO', TRUE);

/*
 * Referente a integração com SuperPay
 */

define('SUPERPAY_ENDERECO_WSDL',    'http://homologacao.superpay.com.br/superpay/servicosPagamentoCompletoWS.Services?wsdl');

define('SUPERPAY_USUARIO',  'ERNET');
define('SUPERPAY_SENHA',    'ERNET');

define('SUPERPAY_CODIGO_ESTABELECIMENTO',    1373891021810);

define('TRANSACAO_STATUS_AUTORIZADO_CONFIRMADO', 1);

/*
 * dmTipoPedido
 */
define('TIPO_PEDIDO_NORMAL',    1);
define('TIPO_PEDIDO_ATRASO',    2);

/*
 * dmStatusPedido
 */
define('STATUS_PEDIDO_CRIADO',                  1);
define('STATUS_PEDIDO_AGUARDANDO_PAGAMENTO',    2);
define('STATUS_PEDIDO_PAGO',                    3);
define('STATUS_PEDIDO_EM_USO',                  4);
define('STATUS_PEDIDO_FINALIZADO',              5);

/* End of file constants.php */
/* Location: ./application/config/constants.php */
