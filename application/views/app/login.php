<?php $this->load->view("app/inc/_header.php")  ?>
<div class="row">
    <div class="col-sm-6 col-md-4 col-md-offset-4 img-login">
        <div class="account-wall">
            <img id="logo" class="img-responsive center-block" src="<?php echo base_url(); ?>assets/app/images/logo.jpg" alt="">
            <form id="frm-login" method="post" class="form-signin center-block" style="width: 80%;">
                    <div class="form-group">
                        <input type="text" autofocus name="login" class="form-control input-login <?php if(form_error('login')) echo 'form_error'; ?>" placeholder="Login" />                    
                    </div>
                    <div class="form-group">
                         <input type="password" name="senha" class="form-control input-password <?php if(form_error('senha')) echo 'form_error'; ?>" placeholder="Senha" />
                    </div>     
                    <button class="btn btn-block button-login" type="submit">Entrar</button>
                <div class="span4">
                    <button type="button" class="btn button-second button-forget" onclick="window.location.href = base_url + 'app/login/recuperar-senha';">Esqueceu a senha?</button>
                    <button type="button" class="btn button-second button-register" onclick="window.location.href = base_url + 'app/cadastro';">Cadastre-se</button>
                </div>   
            </form>
        </div>
    </div>
</div>
<?php $this->load->view("app/inc/_footer.php")  ?>