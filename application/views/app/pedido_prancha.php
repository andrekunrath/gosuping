<?php $this->load->view("app/inc/_header.php")  ?>
<?php $this->load->view("app/inc/_publicidade.php")  ?>
<div class="row paddle-pass-clean">
    <div class="col-sm-6 col-md-4 col-md-offset-4">
        
    </div>
</div>
<div class="row paddle-pass paddle-pass-adquira">
        <div class="col-sm-6 col-md-4 col-md-offset-4 box-buy-description">
            <div>
                <div class="pedido-paddle-pass">
                    <label>
                        Agora escolha uma das portas que estejam disponíveis para retirar a prancha, o colete e o remo                       
                    </label>
                </div>
                <div class="pedido-logos">
                    <img id="pedido-logo-gosuping" class="img-responsive pull-right" src="<?php echo base_url(); ?>assets/app/images/pedido-logo-gosuping.png" alt="">
                    <img id="pedido-logo-mormaii" class="img-responsive pull-right" src="<?php echo base_url(); ?>assets/app/images/logo_mormaii.png" alt="">
                </div>
            </div>

        </div>
        <div class="box-select-item">
            <form id="frmConfirmar" method="post" action="<?php echo base_url('app/pedido/confirmar') ?>">
                <div class="item available">
                    <label>1</label>
                    <input name="prancha" type="radio" value="1">
                </div>
                <div class="item available">
                    <label>2</label>
                    <input name="prancha" type="radio" value="2">
                </div>
                <div class="item available">
                    <label>3</label>
                    <input name="prancha" type="radio" value="3">
                </div>
                <div class="item available">
                    <label>4</label>
                    <input name="prancha" type="radio" value="4">
                </div>
                <div class="item unavailable">
                    <label>5</label>
                    <input name="prancha" type="radio" value="5">
                </div>
                <div class="item available">
                    <label>6</label>
                    <input name="prancha" type="radio" value="6">
                </div>
                <div class="box-buttom">
                    <button class="btn confirm" type="submit">CONFIRMAR</button>
                </div>
            </form>
        </div>
</div>    
<?php $this->load->view("app/inc/_footer.php")  ?> 