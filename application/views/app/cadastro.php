<?php $this->load->view("app/inc/_header.php")  ?>
<div class="row">
	<div class="col-sm-6 col-md-4 col-md-offset-4">
		<div class="account-wall">
			<a href="<?php echo base_url(); ?>"><img id="logo-min" class="img-responsive pull-right" src="<?php echo base_url(); ?>assets/app/images/logo-min.png" alt=""></a>
			<form method="post" class="form-register center-block" style="width: 90%">
				<div class="component">
					<div class="control-group cpf">
					  <label class="control-label" for="textinput">CPF:</label>
					  <div class="controls">
						  <input id="cpf" name="cpf" type="text" maxlength="11" class="form-control bfh-number <?php if(form_error('cpf')) echo 'form_error'; ?>" placeholder="Apenas Números" value="<?php echo set_value('cpf'); ?>">
					  </div>
					</div>
				</div>  
				<div class="component">
					<div class="control-group">
					  <label class="control-label" for="textinput">E-mail:</label>
					  <div class="controls">
						<input id="email" name="email" type="text" class="form-control <?php if(form_error('email')) echo 'form_error'; ?>" placeholder="Digite seu E-mail" value="<?php echo set_value('email'); ?>">
					  </div>
					</div>
				</div> 
				<div class="component">
					<div class="control-group">
					  <label class="control-label" for="textinput">Repetir E-mail:</label>
					  <div class="controls">
						<input id="repetir_email" name="repetir_email" type="text" class="form-control <?php if(form_error('repetir_email')) echo 'form_error'; ?>" placeholder="Digite seu E-mail" value="<?php echo set_value('repetir_email'); ?>">
					  </div>
					</div>
				</div> 
				<div class="component">
					<div class="control-group">
					  <label class="control-label" for="textinput">Senha:</label>
					  <div class="controls">
						  <input id="senha" name="senha" maxlength="6" type="password" class="form-control <?php if(form_error('senha')) echo 'form_error'; ?>" placeholder="6 digitos">
					  </div>
					</div>
				</div> 
				<div class="component">
					<div class="control-group">
					  <label class="control-label" for="textinput">Repetir Senha:</label>
					  <div class="controls">
						  <input id="repetir_senha" maxlength="6"  name="repetir_senha" type="password" class="form-control <?php if(form_error('repetir_senha')) echo 'form_error'; ?>"   placeholder="6 digitos">
					  </div>
					</div>
				</div> 
				<div class="component">
					<div class="control-group">
					  <label class="control-label" for="textinput">Nome Completo:</label>
					  <div class="controls">
						<input id="nome_completo" name="nome_completo" type="text" class="form-control <?php if(form_error('nome_completo')) echo 'form_error'; ?>" placeholder="Nome Completo" value="<?php echo set_value('nome_completo'); ?>">
					  </div>
					</div>
				</div>                     
				<div class="component">
					<div class="control-group">
						<label class="control-label" for="textinput">Data de Nascimento:</label>
						<div class="form-inline date">
							<input id="dia" type="text" name="dia" maxlength="2" class="form-control day <?php if(form_error('dia')) echo 'form_error'; ?>" style="width:25%;" placeholder="DD" value="<?php echo set_value('dia'); ?>" />
							<input id="mes" type="text" name="mes" maxlength="2" class="form-control month <?php if(form_error('mes')) echo 'form_error'; ?>" style="width:25%;" placeholder="MM" value="<?php echo set_value('mes'); ?>" />
							<input id="ano" type="text" name="ano" maxlength="4" class="form-control <?php if(form_error('ano')) echo 'form_error'; ?>" style="width:35%;" placeholder="AAAA" value="<?php echo set_value('ano'); ?>" />
						</div>
					</div>
				</div>
				<div class="component">
					<div class="control-group">
						<label class="control-label" for="textinput">Celular com DDD:</label>
						<div class="form-inline">
							<input id="ddd" type="text" name="ddd" maxlength="2" class="form-control ddd <?php if(form_error('ddd')) echo 'form_error'; ?>" style="width:25%" placeholder="DDD" value="<?php echo set_value('ddd'); ?>" />
							<input id="celular" type="text" name="celular" class="form-control <?php if(form_error('celular')) echo 'form_error'; ?>" style="width:60%" placeholder="Seu Número" value="<?php echo set_value('celular'); ?>" />
						</div>
					</div>
				</div>
				<div class="component">
					<div class="control-group">
					  <label class="control-label" for="textinput">Sexo:</label>
						<div class="form-inline">
						   <label class="sex">
                                                       <input type="radio" checked  name="sexo"> <div>Masculino</div>
							</label>     
							<label class="sex">
                                                            <input type="radio"name="sexo"> <div class="famele">Feminino &nbsp;</div>
						   </label>
						</div>
					</div>
				</div> 
				<div class="component">
					<div class="control-group">
					  <label class="control-label" for="textinput">CEP:</label>
					  <div class="controls">
						  <input id="cep" name="cep"  maxlength="8" type="text"  style="width:75%" class="form-control <?php if(form_error('cep')) echo 'form_error'; ?>" placeholder="Apenas Números" value="<?php echo set_value('cep'); ?>" />
					  </div>
					</div>
				</div> 
				<button class="btn btn-block button-login btn-register" type="submit">Cadastrar</button>
			</form>
		</div>
	</div>
</div>
<?php $this->load->view("app/inc/_footer.php")  ?>

<!-- javascripts -->
<script src="<?php echo base_url() ?>assets/app/scripts/lib/jquery.mask.min.js"></script>
<script src="<?php echo base_url() ?>assets/app/scripts/cadastro.js"></script>