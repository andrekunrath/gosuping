<?php $this->load->view("app/inc/_header.php")  ?>
<?php $this->load->view("app/inc/_publicidade.php")  ?>
<div class="row paddle-pass-clean">
    <div class="col-sm-6 col-md-4 col-md-offset-4">
        
    </div>
</div>
<form class="form-horizontal" role="form" method="post">
    <div class="row paddle-pass">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div>
                <div class="pedido-paddle-pass">
                     <label class="control-label" for="textinput">Selecione seu Peddle Pass:</label>
                        <div class="form-inline">
                           <label>
                               <input type="radio" checked class="radio-price" name="valor_hora" value="4500"> <div class="price">R$ 45,00 valor da hora</div>
                            </label> 
                           <label class="pedido-description">
                               Seu Pedido Pass será renovado e cobrado
                               <b>automaticamente por mais 2 horas,</b> <br/>
                               após este tempo é obrigatório a devolução do eqipamento.<br/> Você terá 
                               <strong>20 minutos</strong> 
                               de tolerância para devolução do equipamento após a primeira hora. 
                            </label>                         
                        </div>    
                    </div>                
                </div>
                <div class="pedido-logos">
                    <img id="pedido-logo-gosuping" class="img-responsive pull-right" src="<?php echo base_url(); ?>assets/app/images/pedido-logo-gosuping.png" alt="">
                    <img id="pedido-logo-mormaii" class="img-responsive pull-right" src="<?php echo base_url(); ?>assets/app/images/logo_mormaii.png" alt="">
                </div>
            </div>

        </div>
    	<div class="row pedido-dados-cartao">
            <div class="col-sm-6 col-md-4 col-md-offset-4 ">           
                <div class="form-group">
                    <label for="inputEmail3" class="control-label text-left">Cartão de crédito</label>
                    <div class="fields">
                        <select class="form-control" name="cartao">
                        	<option value="120">Visa</option>
							<option value="121">Mastercard</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="control-label">Nome Impresso</label>
                    <div class="fields">
                      <input type="text" class="form-control <?php if(form_error('nome_impresso')) echo 'form_error'; ?>" id="inputPassword3" name="nome_impresso" value="<?php echo set_value('nome_impresso'); ?>">
                    </div>
                  </div>
                <div class="form-group">
                    <label for="inputEmail3" class="control-label">Número do cartão</label>
                    <div class="fields">
                      <input type="text" class="form-control numero_cartao <?php if(form_error('numero_cartao')) echo 'form_error'; ?>" id="inputPassword3" name="numero_cartao" maxlength="16" value="<?php echo set_value('numero_cartao'); ?>">
                    </div>
                  </div>
                <div class="form-group">
                    <label for="inputEmail3" class="control-label">Data do Vencimento</label>
					<div class="fields" >
						<input type="text" class="form-control pedido-validate-month  <?php if(form_error('data_validade_mes')) echo 'form_error'; ?>" name="data_validade_mes" placeholder="MM" value="<?php echo set_value('data_validade_mes'); ?>">
						<input type="text" class="form-control pedido-validate-year  <?php if(form_error('data_validade_ano')) echo 'form_error'; ?>" name="data_validade_ano" placeholder="YYYY" value="<?php echo set_value('data_validade_ano'); ?>">
                    </div>
                </div>  
                <div class="form-group">
                    <label for="inputEmail3" class="control-label">Código de Segurança</label>
                    <div class="fields">
                    	<input type="text" class="form-control codigo-cvc  <?php if(form_error('codigo_seguranca')) echo 'form_error'; ?>" id="inputPassword3" name="codigo_seguranca" value="<?php echo set_value('codigo_seguranca'); ?>">
                    </div>
                  </div>  
                <div class="form-group">
                    <label class="control-label terms">
                        <input type="checkbox" name="termos"> <div>Li e aceito os Termos e Condições do GoSuping.</div>
                    </label>
                </div>
                <div class="form-group">
                    <button class="btn btn-terms" onclick="window.location = base_url + 'app/termos';" type="button">
                    Termos e Condições</button>
                    <button class="btn btn-continue" type="submit">Continuar</button>
                </div>                 
            </div>
    	</div>
</form>    
<?php $this->load->view("app/inc/_footer.php")  ?>

<!-- javascripts -->
<script src="<?php echo base_url() ?>assets/app/scripts/lib/jquery.mask.min.js"></script>
<script src="<?php echo base_url() ?>assets/app/scripts/pedido.js"></script>