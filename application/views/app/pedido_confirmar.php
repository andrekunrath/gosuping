<?php $this->load->view("app/inc/_header.php")  ?>
<?php $this->load->view("app/inc/_publicidade.php")  ?>

<div class="row pedido-confirmar">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="pedido-logos end">
                <img id="pedido-logo-gosuping" class="img-responsive pull-right" src="<?php echo base_url(); ?>assets/app/images/fim_gosuping.png" alt="">
                <img id="pedido-logo-mormaii" class="img-responsive pull-right" src="<?php echo base_url(); ?>assets/app/images/fim_mormaii.png" alt="">
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-md-offset-4 text-end">
                <label>
                    Você encontra-se em frente ao quiosque? Clicando em "sim", você destranca a porta e o equipamento fica exposto.
                    <br><br>Deseja continuar?
                </label>
            <form method="post" action="<?php echo base_url('app/pedido/fim'); ?>">
                <div class="box-buttom">
                    <button class="btn btn-warning" type="button" onclick="window.location = '<?php echo base_url('app/meus-pedidos'); ?>';">CANCELAR</button>
                    <button class="btn confirm" type="submit">CONFIRMAR</button>
                </div>
            </form>
        </div>
<div>
<?php $this->load->view("app/inc/_footer.php")  ?> 