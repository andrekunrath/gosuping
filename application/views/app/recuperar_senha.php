<?php $this->load->view("app/inc/_header.php")  ?>
<div class="row">
    <div class="col-sm-6 col-md-4 col-md-offset-4 img-forget">
        <div class="account-wall box-img-form-forget-pass">            
            <div class="box-form-forget-pass">
            	<?php
            	if($sucesso)
            	{ 
            	?>
            		<div class="box-sucesso">
	            		<p>Sua nova senha foi enviada para o seu e-mail!</p>
	            		<button type="button" onclick="window.location.href = base_url + 'app/login';" class="btn btn-voltar">Voltar</button>
            		</div>
            	<?php
            	}
            	else
            	{
            	?>
	                <form id="frm-esqueci-senha" method="post" class="form-signin center-block" style="width: 70%;">
	                        <label for="inputEmail3" class="forget-label control-label">Esqueceu a senha?</label>
	                        <div class="form-group">
	                            <div class="fields">
	                                <input type="text" name="email" placeholder="Digite seu E-mail cadastrado" class="form-control <?php if(form_error('email')) echo 'form_error'; ?>" />
	                            </div>
	                        </div>  
	                        <div class="form-group">
	                            <button type="button" onclick="window.location.href = base_url + 'app/login';" class="btn">Cancelar</button>
	                            <button type="submit" class="btn send-pass">Enviar Senha</button>
	                        </div>                       
	                </form>
                <?php 
            	}
                ?>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("app/inc/_footer.php")  ?>