<?php $this->load->view("app/inc/_header.php"); ?>
<?php $this->load->view("app/inc/_publicidade.php"); ?>

<div class="row paddle-pass-clean">
    <div class="col-sm-6 col-md-4 col-md-offset-4">
        
    </div>
</div>
<div class="row">
    <div class="col-sm-6 col-md-4 col-md-offset-4">
        <div class="table-responsive">
            <table class="table table-bordered table-order">
                <thead>
                    <tr>
                        <th>Código pedido</th>
                        <th>Data</th>
                        <th>Utilizar</th>
                    </tr>
                </thead>
                <tbody>
                    <form method="post">
                        <?php
                        if (count($colecaoPedido))
                        {
                            foreach ($colecaoPedido as $pedido)
                            {
                                $date = date_create($pedido->DataCriacao);
                        ?>

                            <tr>
                                <td><?php echo $pedido->PedidoID; ?></td>
                                <td><?php echo date_format($date, "d/m/Y h:i:s"); ?></td>
                                <td>
                                    <?php
                                    if ($pedido->StatusPedidoID == STATUS_PEDIDO_PAGO)
                                    {
                                    ?>
                                        <button type="submit" class="btn btn-default" title="Utilizar" name="pass" value="<?php echo $pedido->PedidoID; ?>">
                                            <span class="glyphicon glyphicon-circle-arrow-down" aria-hidden="true"></span>
                                        </button>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                        <span>Já utilizado</span>
                                    <?php
                                    }
                                    ?>
                                </td>
                            </tr>

                        <?php
                            }
                        }
                        ?>
                    </form>
                </tbody>
            </table>
        </div>
        <div class="box-buy-pass box-buy-pass-order">
            <a href="<?php echo base_url('app/pedido'); ?>" class="btn btn-primary btn-lg active" role="button">CLIQUE AQUI <br>  COMPRE O SEU <br>  PEDIDO PADDLE PASS</a>
        </div>         
    </div>
</div>

<?php $this->load->view("app/inc/_footer.php")  ?> 