<?php $this->load->view("app/inc/_header.php")  ?>
<div class="row">
    <div class="col-sm-6 col-md-4 col-md-offset-4">
        <div class="pedido-logos end">
            <img id="pedido-logo-gosuping" class="img-responsive pull-right" src="<?php echo base_url(); ?>assets/app/images/fim_gosuping.png" alt="">
            <img id="pedido-logo-mormaii" class="img-responsive pull-right" src="<?php echo base_url(); ?>assets/app/images/fim_mormaii.png" alt="">
        </div>
    </div>
    <div class="col-sm-6 col-md-4 col-md-offset-4 text-end">
        <label>
            Esta aplicação só está disponível para plataformas mobile.
        </label>
    </div>
    <div>
<?php $this->load->view("app/inc/_footer.php")  ?>