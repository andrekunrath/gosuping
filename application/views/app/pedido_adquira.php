<?php $this->load->view("app/inc/_header.php")  ?>
<?php $this->load->view("app/inc/_publicidade.php")  ?>
<div class="row paddle-pass-clean">
    <div class="col-sm-6 col-md-4 col-md-offset-4">
        
    </div>
</div>
<div class="row paddle-pass paddle-pass-adquira">
        <div class="col-sm-6 col-md-4 col-md-offset-4 box-buy-description">
            <div>
                <div class="pedido-paddle-pass">
                    <label>
                        Adquira um paddle pass clicando no botão comprar e tenha uma boa remada.                        
                    </label>
                </div>
                <div class="pedido-logos">
                    <img id="pedido-logo-gosuping" class="img-responsive pull-right" src="<?php echo base_url(); ?>assets/app/images/pedido-logo-gosuping.png" alt="">
                    <img id="pedido-logo-mormaii" class="img-responsive pull-right" src="<?php echo base_url(); ?>assets/app/images/logo_mormaii.png" alt="">
                </div>
            </div>

        </div>
        <div class="box-buy-pass">
            <a href="<?php echo base_url('app/pedido'); ?>" class="btn btn-primary btn-lg active" role="button">CLIQUE AQUI <br>  COMPRE O SEU <br>  PEDIDO PADDLE PASS</a>
        </div>  
</div>
<?php $this->load->view("app/inc/_footer.php")  ?>