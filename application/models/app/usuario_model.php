<?php

class Usuario_model extends CI_Model 
{
	function __construct() 
	{
		parent::__construct();
	}
	
	function do_login($email, $senha)
	{
        $this->db->select('*');
        $this->db->from('Usuario');
        $this->db->where('Email', $email);
        $this->db->where('Senha', $senha);

        return $this->db->get()->row();
    }
    
    function insert_usuario($data) 
    {
    	$this->db->insert('Usuario', $data);
    	return $this->db->insert_id();
    }
    
    function get($data = array())
    {
    	$this->db->from('Usuario');
    	
    	if(isset($data['Email']))
    	{
    		$this->db->where('Email', $data['Email']);
    	}
    	
    	return $this->db->get()->row();
    }
    
    function update($id, $data = array())
    {
    	$this->db->where('UsuarioID', $id);
    	$this->db->update('Usuario', $data);
    }
}