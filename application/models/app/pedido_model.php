<?php

class Pedido_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insert($data)
    {
        $this->db->insert('Pedido', $data);

        return $this->db->insert_id();
    }

    function update($id, $data = array())
    {
        $this->db->where('PedidoID', $id);
        $this->db->update('Pedido', $data);
    }

    function update_transacao($id, $data = array())
    {
        $this->db->where('TransacaoID', $id);
        $this->db->update('Pedido', $data);
    }

    function get_pedidos($busca = array())
    {
        $this->db->select('Pedido.*');
        $this->db->from('Pedido');
        $this->db->where('UsuarioID', $busca['UsuarioID']);
        $this->db->where('TipoPedidoID', 1);
        $this->db->order_by('DataCriacao','Desc');

        return $this->db->get()->result();
    }

    function get($busca)
    {
        $this->db->select('Pedido.*');
        $this->db->from('Pedido');

        if(isset($busca['PedidoID']))
        {
            $this->db->where('PedidoID', $busca['PedidoID']);
        }

        if(isset($busca['UsuarioID']))
        {
            $this->db->where('UsuarioID', $busca['UsuarioID']);
        }

        return $this->db->get()->row();
    }
}