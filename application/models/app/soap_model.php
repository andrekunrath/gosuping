<?php

class Soap_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	var $USUARIO_SUPERPAY = SUPERPAY_USUARIO; // Informado pelo suporte
	var $SENHA_SUPERPAY = SUPERPAY_SENHA; // Informado pelo suporte
	var $CODIGO_ESTABELECIMENTO = SUPERPAY_CODIGO_ESTABELECIMENTO;
	
	function consultaTransacaoEspecifica($dados_envio){
		$dados_envio['codigoEstabelecimento'] = $this->CODIGO_ESTABELECIMENTO;
		$parametros = array('consultaTransacaoWS'=>$dados_envio, 'usuario'=>$this->USUARIO_SUPERPAY, 'senha'=>$this->SENHA_SUPERPAY);
		$funcao_chamada = 'consultaTransacaoEspecifica';
		$retorno = callWebServices($parametros,$funcao_chamada, SUPERPAY_ENDERECO_WSDL);
		return $retorno;		
	}

	function pagamentoCompleto($dados_envio){
		$dados_envio['codigoEstabelecimento'] = $this->CODIGO_ESTABELECIMENTO;
		$parametros = array('transacao'=>$dados_envio, 'usuario'=>$this->USUARIO_SUPERPAY, 'senha'=>$this->SENHA_SUPERPAY);
		$funcao_chamada = 'pagamentoTransacaoCompleta';
		$retorno = callWebServices($parametros,$funcao_chamada, SUPERPAY_ENDERECO_WSDL);
		return $retorno;
	}
	
	function pagamentoCompletoMaisCartao($dados_envio){
		$dados_envio['codigoEstabelecimento'] = $this->CODIGO_ESTABELECIMENTO;
		$parametros = array('transacao'=>$dados_envio, 'usuario'=>$this->USUARIO_SUPERPAY, 'senha'=>$this->SENHA_SUPERPAY);
		$funcao_chamada = 'pagamentoTransacaoCompletaMaisCartoesCredito';
		$retorno = callWebServices($parametros,$funcao_chamada, SUPERPAY_ENDERECO_WSDL);
		return $retorno;
	}
	
	function operacaoTransacao($dados_envio){
		$dados_envio['codigoEstabelecimento'] = $this->CODIGO_ESTABELECIMENTO;
		$parametros = array('operacao'=>$dados_envio, 'usuario'=>$this->USUARIO_SUPERPAY, 'senha'=>$this->SENHA_SUPERPAY);
		$funcao_chamada = 'operacaoTransacao';
		$retorno = callWebServices($parametros,$funcao_chamada, SUPERPAY_ENDERECO_WSDL);
		return $retorno;
	}
	
	function criarRecorrencia($dados_envio){		
		$parametros = array('recorrenciaWS'=>$dados_envio, 'usuario'=>array("usuario"=>$this->USUARIO_SUPERPAY, 'senha'=>$this->SENHA_SUPERPAY));
		$funcao_chamada = 'cadastrarRecorrenciaWS';
		$retorno = callWebServices($parametros,$funcao_chamada,"http://homologacao.superpay.com.br/superpay/servicosRecorrenciaWS.Services?wsdl");
		return $retorno;
	}
	
	function consultarRecorrencia($dados_envio){		
		$parametros = array('recorrenciaConsultaWS'=>$dados_envio, 'usuario'=>array("usuario"=>$this->USUARIO_SUPERPAY, 'senha'=>$this->SENHA_SUPERPAY));
		$funcao_chamada = 'consultaTransacaoRecorrenciaWS';
		$retorno = callWebServices($parametros,$funcao_chamada,"http://homologacao.superpay.com.br/superpay/servicosRecorrenciaWS.Services?wsdl");
		return $retorno;
	}
	
	function cancelarRecorrencia($dados_envio){
		$parametros = array('recorrenciaCancelarWS'=>$dados_envio, 'usuario'=>array("usuario"=>$this->USUARIO_SUPERPAY, 'senha'=>$this->SENHA_SUPERPAY));
		$funcao_chamada = 'cancelarRecorrenciaWS';
		$retorno = callWebServices($parametros,$funcao_chamada,"http://homologacao.superpay.com.br/superpay/servicosRecorrenciaWS.Services?wsdl");
		return $retorno;
	}

}
