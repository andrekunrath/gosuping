<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cadastro extends CI_Controller
{
	function __construct() 
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('util_helper');
        $this->load->model('app/usuario_model');
    }

    public function Index()
    {
    	if($this->input->post())
    	{
    		// remove as máscaras do post
    		remover_mascara($_POST, array('cpf', 'celular', 'cep'));
    		
	    	// validação do form
	    	$this->form_validation->set_rules('cpf', 'CPF', 'required|max_length[11]|is_unique[Usuario.CPF]');
	    	$this->form_validation->set_rules('email', 'E-mail', 'required|xss_clean');
	    	$this->form_validation->set_rules('repetir_email', 'Repetir E-mail', 'required|matches[email]|xss_clean');
	    	$this->form_validation->set_rules('senha', 'Senha', 'required|min_length[6]|md5');
	    	$this->form_validation->set_rules('repetir_senha', 'Repetir senha', 'required|min_length[6]|matches[senha]');
	    	$this->form_validation->set_rules('nome_completo', 'Nome completo', 'required|xss_clean');
	    	$this->form_validation->set_rules('dia', 'Dia', 'required|max_length[2]');
	    	$this->form_validation->set_rules('mes', 'Mês', 'required|max_length[2]');
	    	$this->form_validation->set_rules('ano', 'Ano', 'required|max_length[4]');
	    	$this->form_validation->set_rules('ddd', 'DDD', 'required|max_length[2]');
	    	$this->form_validation->set_rules('celular', 'Celular', 'required|max_length[9]');
	    	$this->form_validation->set_rules('sexo', 'Sexo', 'required');
	    	$this->form_validation->set_rules('cep', 'CEP', 'required|max_length[8]');
	    	
	    	if ($this->form_validation->run() == TRUE)
	    	{
	    		$usuario = array();
	    		
	    		$usuario['Tipo'] = 1;
	    		$usuario['Nome'] = $this->input->post('nome_completo');
	    		$usuario['DataNascimento'] = $this->input->post('ano') . '-' . $this->input->post('mes') . '-' . $this->input->post('dia');
	    		$usuario['Email'] = $this->input->post('email');
	    		$usuario['Senha'] = $this->input->post('senha');
	    		$usuario['CPF'] = $this->input->post('cpf');
	    		$usuario['CEP'] = $this->input->post('cep');
	    		$usuario['CelularDDD'] = $this->input->post('ddd');
	    		$usuario['Celular'] = $this->input->post('celular');
	    		$usuario['Sexo'] = $this->input->post('sexo');

	    		$usuario['UsuarioID'] = $this->usuario_model->insert_usuario($usuario);
	    		
	    		// redireciona para a tela "meus pedidos" com o novo usuário já logado
	    		$usuario = $this->usuario_model->do_login($usuario['Email'], $usuario['Senha']);

	    		if (is_object($usuario))
	    		{
	    			$this->nativesession->set('usuario_logado', $usuario);
	    			
	    			redirect(base_url('app/meus-pedidos/'));
	    		}
	    	}
    	}
    	
    	$this->load->view('app/cadastro');
    }
}