<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pedido extends Base_Controller
{
	function __construct() 
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('webservice');
        $this->load->helper('socket');
        $this->load->model('app/soap_model');
        $this->load->model('app/pedido_model');
    }
    
    public function Index()
    {
    	if($this->input->post())
    	{
    		$this->form_validation->set_rules('cartao', 'Cartão', 'required');
    		$this->form_validation->set_rules('nome_impresso', 'Nome impresso', 'required');
    		$this->form_validation->set_rules('numero_cartao', 'Número do cartão', 'required');
    		$this->form_validation->set_rules('data_validade_mes', 'Mês de validade', 'required');
    		$this->form_validation->set_rules('data_validade_ano', 'Ano de validade', 'required');
    		$this->form_validation->set_rules('codigo_seguranca', 'Código de segurança', 'required');
    		$this->form_validation->set_rules('termos', 'Termos de condição', 'required');
    		
    		if ($this->form_validation->run() == TRUE)
    		{
                // insert do pedido no banco
                $data_pedido = array();

                $data_pedido['ProdutoID'] = 1;
                $data_pedido['UsuarioID'] = $this->usuario_logado->UsuarioID;
                $data_pedido['PedidoAdmin'] = 0;
                $data_pedido['StatusPedidoID'] = STATUS_PEDIDO_AGUARDANDO_PAGAMENTO;
                $data_pedido['DataCriacao'] = date(FORMATO_DATA);
                $data_pedido['TipoPedidoID'] = 1;
                $data_pedido['PedidoID'] = $this->pedido_model->insert($data_pedido);

                // requisição para o webservice SuperPay
                $dados_envio["numeroTransacao"] = $data_pedido['PedidoID'];
                $dados_envio["nomeTitularCartaoCredito"] = $this->input->post('nome_impresso');
                $dados_envio["numeroCartaoCredito"] = $this->input->post('numero_cartao');
                $dados_envio["codigoSeguranca"] = $this->input->post('codigo_seguranca');
                $dados_envio["dataValidadeCartao"] = $this->input->post('data_validade_mes') . '/' . $this->input->post('data_validade_ano'); //mm/yyyy
                $dados_envio["parcelas"] = 1;
                $dados_envio["codigoFormaPagamento"] = $this->input->post('cartao');
                $dados_envio["valor"] = $this->input->post('valor_hora');
                $dados_envio["IP"] = get_client_ip();
                $dados_envio["idioma"] = "1";
                $dados_envio["origemTransacao"] = "1"; // origemTransacao (1 = eCommerce, 2 = Mobile, 3 = URA, 4 = POS
                $dados_envio["taxaEmbarque"] = "0"; // taxaEmbarque usado por companhia aerea
                $dados_envio["urlCampainha"] = base_url('app/pedido/campainha'); // urlCampainha: este é o endereço chamado quando há a alteração de status de um pedido enviando para ele o codigoEstabelecimento e numeroTransacao
                $dados_envio["urlRedirecionamentoNaoPago"]; // usada somente no modo redirect, deve ser null
                $dados_envio["urlRedirecionamentoPago"]; // usada somente no modo redirect, deve ser null

                $dados_envio["dadosUsuarioTransacao"]["nomeComprador"] = $this->usuario_logado->Nome;
                $dados_envio["dadosUsuarioTransacao"]["cepEnderecoComprador"] = $this->usuario_logado->CEP;
                $date = date_create($this->usuario_logado->DataNascimento);
                $dados_envio["dadosUsuarioTransacao"]["dataNascimentoComprador"] = date_format($date, "d/m/Y");
                $dados_envio["dadosUsuarioTransacao"]["tipoCliente"] = 1; // tipoCliente (1 = Física, 2 = Jurídica)
                $dados_envio["dadosUsuarioTransacao"]["documentoComprador"] =  $this->usuario_logado->CPF;  // o campo documentoComprador é o documento principal para a identificação do comprador como por exemplo cpf/cnpj
                $dados_envio["dadosUsuarioTransacao"]["emailComprador"] = $this->usuario_logado->Email;
                $dados_envio["dadosUsuarioTransacao"]["sexoComprador"] = "m";
                $dados_envio["dadosUsuarioTransacao"]["codigoCliente"] = $this->usuario_logado->UsuarioID;
                $dados_envio["dadosUsuarioTransacao"]["codigoTipoTelefoneComprador"] = 1;
                $dados_envio["dadosUsuarioTransacao"]["dddComprador"] = $this->usuario_logado->CelularDDD;
                $dados_envio["dadosUsuarioTransacao"]["telefoneComprador"] = $this->usuario_logado->Celular;

                // teremos um único produto
                $dados_envio["itensDoPedido"][0]["codigoProduto"] = "1";
                $dados_envio["itensDoPedido"][0]["codigoCategoria"] = "1";
                $dados_envio["itensDoPedido"][0]["nomeProduto"] = "Paddle Pass";
                $dados_envio["itensDoPedido"][0]["quantidadeProduto"] = "1";
                $dados_envio["itensDoPedido"][0]["valorUnitarioProduto"] = 4500;
                $dados_envio["itensDoPedido"][0]["nomeCategoria"] = "Ticket";

                $soap = new Soap_model();
                $retorno = $soap->pagamentoCompleto($dados_envio);

                $data_pedido['StatusTransacaoID'] = $retorno['return']['statusTransacao'];
                $data_pedido['TransacaoID'] = $retorno['return']['numeroTransacao'];
                $this->pedido_model->update($data_pedido['PedidoID'], $data_pedido);

                if ($data_pedido['StatusTransacaoID'] == TRANSACAO_STATUS_AUTORIZADO_CONFIRMADO || TESTAR_FLUXO_PEDIDO)
                {
                    $data_pedido['StatusPedidoID'] = STATUS_PEDIDO_PAGO;
                    $this->pedido_model->update($data_pedido['PedidoID'], $data_pedido);

                    // armazena em sessão o número do pedido que o usuário poderá utilizra para liberar uma prancha
                    $this->nativesession->set('pass', $retorno['return']['numeroTransacao']);
                    redirect(base_url('app/pedido/sucesso'));
                }
    		}
    	}
    	
    	$this->load->view('app/pedido');
    }

    public function Historico()
    {
        // se o usuário selecionou um pass válido, salva em sessão e redireciona para tela de seleção de pranchas
        if($this->input->post())
        {
            $pass = $this->input->post('pass');

            // se o pass for válido, redireciona para a tela de seleção de pranchas
            if($this->Validar($pass))
            {
                $this->nativesession->set('pass', $pass);

                redirect(base_url('app/pedido/prancha'));
            }
            else // se não for valido, exibir erro
            {
                $this->load->view('app/pedido_erro', array('mensagem' => 'Erro ao validar o pass'));
            }
        }
        else
        {
            $busca = array();
            $busca['UsuarioID'] = $this->usuario_logado->UsuarioID;
            $busca['TipoPedidoID'] = TIPO_PEDIDO_NORMAL;

            $data = array();
            $data['colecaoPedido'] = $this->pedido_model->get_pedidos($busca);

            $this->load->view('app/meus_pedidos', $data);
        }
    }

    public function Adquira()
    {
    	$this->load->view('app/pedido_adquira');        
    }    
    
    public function Sucesso()
    {
        $this->load->view('app/pedido_sucesso');
    }        
    
    public function Prancha()
    {
        $pass = $this->nativesession->get('pass');

        // para acessar esta tela, o usuário deve ter um pass válido em sessão (veio da tela de pagamento ou veio dos meus pedidos selecionando um pass válido
        if (empty($pass) || !$this->Validar($pass))
        {
            redirect(base_url('app/meus-pedidos'));
        }

    	$this->load->view('app/pedido_prancha');
    }

    public function Confirmar()
    {
        $prancha = $this->input->post('prancha');
        $pass = $this->nativesession->get('pass');

        if (empty($pass) || empty($prancha) || !$this->Validar($pass))
        {
            redirect(base_url('app/meus-pedidos'));
        }

        $this->nativesession->set('prancha', $this->input->post('prancha'));

        $this->load->view('app/pedido_confirmar');
    }
    
    public function Fim()
    {
        $prancha = $this->nativesession->get('prancha');
        $pass = $this->nativesession->get('pass');

        if (empty($prancha) || empty($pass) || !$this->Validar($pass))
        {
            redirect(base_url('app/meus-pedidos'));
        }

        $retorno = liberar_box($prancha);

        if($retorno)
        {
            $pedido = array();

            $pedido['PedidoID'] = $pass;
            $pedido['StatusPedidoID'] = STATUS_PEDIDO_EM_USO;
            $pedido['DataInicioUso'] =  date(FORMATO_DATA);

            $this->pedido_model->update($pedido['PedidoID'] ,$pedido);

            $this->nativesession->delete('prancha');
            $this->nativesession->delete('pass');

            $this->load->view('app/pedido_fim_sucesso');
        }
        else
        {
            $this->load->view('app/pedido_erro', array('mensagem' => 'Ocorreu um erro na comunicação com a caixa'));
        }
    }

    public function Termos()
    {
        $this->router->class = 'termos';
        $this->load->view('app/termos');
    }

    public function Campainha()
    {
        $dados_envio = array();
        $dados_envio['numeroTransacao'] = $this->input->post('numeroTransacao');

        $soap = new Soap_model();
        $retorno = $soap->consultaTransacaoEspecifica($dados_envio);

        $data_pedido = array();

        $data_pedido['TransacaoID'] = $retorno['return']['numeroTransacao'];
        $data_pedido['StatusTransacaoID'] = $retorno['return']['statusTransacao'];

        if($data_pedido['StatusTransacaoID'] == TRANSACAO_STATUS_AUTORIZADO_CONFIRMADO)
        {
            $data_pedido['StatusPedidoID'] = STATUS_PEDIDO_PAGO;
        }

        $this->pedido_model->update_transacao($data_pedido['TransacaoID'], $data_pedido);
    }

    /**
     * Função para validar se o pass está válido e se pertence ao usuário que está logado
     */
    private function Validar($pass)
    {
        $busca = array();

        $busca['PedidoID'] = $pass;
        $busca['UsuarioID'] = $this->usuario_logado->UsuarioID;

        $pedido = $this->pedido_model->get($busca);

        if ($pedido == null)
        {
            return false;
        }

        if ($pedido->StatusPedidoID != STATUS_PEDIDO_PAGO)
        {
            return false;
        }

        return  true;
    }
}