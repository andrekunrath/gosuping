<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{
	protected $ci;
	
	function __construct() 
    {
        parent::__construct();

		verificaAgent();

        $this->load->library('form_validation');
        $this->load->model('app/usuario_model');
        $this->load->helper('util_helper');
        $this->ci = &get_instance();
    }
    
    public function Index()
    {
    	if ($this->input->post())
    	{
    		// validação do form
    		$this->form_validation->set_rules('login', 'Login', 'required');
    		$this->form_validation->set_rules('senha', 'Senha', 'required');
    		
    		if ($this->form_validation->run() == TRUE)
    		{
    			$email = $this->input->post('login');
    			$senha = md5($this->input->post('senha'));
    			
    			$usuario = $this->usuario_model->do_login($email, $senha);

    			if (is_object($usuario))
    			{
    				$this->nativesession->set('usuario_logado', $usuario);
                    $this->nativesession->set('usuario_passo', 'adquira');
                                
    				redirect(base_url('app/meus-pedidos/'));
    			}
    		}
    	}
    	$this->load->view('app/login');
    }
    
    public function RecuperarSenha()
    {
    	$data = array();
    	
    	$data['sucesso'] = false;
    	
    	if ($this->input->post())
    	{
	    	$this->form_validation->set_rules('email', 'E-mail', 'required|xss_clean');
	    	
	    	if ($this->form_validation->run() == TRUE)
	    	{
	    		// disparar e-mail para o usuário
	    		$post = $this->input->post();
	    		
	    		$usuario = $this->usuario_model->get($post);
	    		
	    		if(is_object($usuario))
	    		{
	    			// gera a nova senha e atualiza o banco
	    			$usuario->Senha = gerar_string_randomica();
	    			$data_update['Senha'] = md5($usuario->Senha);
	    			
	    			$this->usuario_model->update($usuario->UsuarioID ,$data_update);
	    			
		    		$this->load->library('email');
					
					$this->email->from(EMAIL_ESQUECI_SENHA_FROM, EMAIL_ESQUECI_SENHA_FROM_NOME);
					$this->email->to($usuario->Email); 
					$this->email->subject(EMAIL_ESQUECI_SENHA_ASSUNTO);
					$this->email->message("A sua nova senha é: $usuario->Senha");
					
					$data['sucesso'] = $this->email->send();
	    		}
	    	}
    	}
    	
    	$this->load->view('app/recuperar_senha', $data);
    }
    
    public function Deslogar()
    {
    	$this->nativesession->delete('usuario_logado');
    	redirect(base_url().'app/login');
    }
}