<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Index extends Base_Controller
{
	function __construct() 
    {
        parent::__construct();
    }
    
    public function Index()
    {
        redirect(base_url().'app/login');
    }
    
}