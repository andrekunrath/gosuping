<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Erroagent extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('util_helper');
        $this->load->model('app/usuario_model');
    }

    public function Index()
    {
        $this->load->view('app/erro_agent');
    }
}