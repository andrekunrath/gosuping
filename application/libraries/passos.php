<?php

class Passos 
{
    private $CI;

    public function __construct() 
    {
        $this->CI = &get_instance();
        $this->CI->load->library('nativesession');
    }

    function verfica($passo)
    {
        $url = base_url();
    	$usuario_passo = $this->CI->nativesession->get('usuario_passo');    
        
        if($usuario_passo != $passo){
            redirect($url.'app/pedido/'.$passo);            
        }             
    }   
    
    private function set($passo)
    {
        $this->nativesession->set('usuario_passo', 'adquira');
    }    
}
