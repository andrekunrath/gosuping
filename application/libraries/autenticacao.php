<?php

/**
 * Classe utilizada para controlar o login do usuário
 * 
 * @author mozart
 */

class Autenticacao 
{
    private $CI;

    public function __construct() 
    {
        $this->CI = &get_instance();
        $this->CI->load->library('nativesession');
    }

    function verificaLogin() 
    {
    	$usuario_logado = $this->CI->nativesession->get('usuario_logado');
    	
    	if($usuario_logado == null)
    	{
    		redirect(base_url()."app/login");
    	}
        else
        {
            return $usuario_logado;
        }
    }    
}
