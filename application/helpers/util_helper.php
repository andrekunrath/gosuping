<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('remover_mascara'))
{
    function remover_mascara(&$array = array() , $campos = array())
    {
    	foreach($campos as $campo)
    	{
    		$array[$campo] = preg_replace('/\D+|(-)/', '', $array[$campo]);
    	}
    }
}

if ( ! function_exists('gerar_string_randomica'))
{
	function gerar_string_randomica()
	{
		return substr(md5(uniqid(rand(1,6))), 0, 8);
	}
}

if ( ! function_exists('dump'))
{
	function dump($dado, $exit = true)
	{
		echo "<pre>";
		print_r($dado);
		echo "</pre>";

		if($exit) exit();
	}
}

if ( ! function_exists('get_client_ip')) {
	function get_client_ip()
	{
		$ipaddress = '';

		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if (getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if (getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if (getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if (getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		else if (getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';

		return $ipaddress;
	}
}

if ( ! function_exists('verificaAgent')) {
	function verificaAgent()
	{
		$CI = &get_instance();

		if (!$CI->agent->is_mobile()) {
			redirect(base_url('app/erroagent'));
		}
	}
}