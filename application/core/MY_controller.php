<?php

/**
 * Controller base, todos os controllers vão extender este (com exceção do login)
 * 
 * @author mozart
 *
 */

class Base_Controller extends CI_Controller 
{
    protected $usuario_logado = null;

    public function __construct() 
    {
        parent::__construct();
        
        $this->usuario_logado = $this->autenticacao->verificaLogin();
    }
}