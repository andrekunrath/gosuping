$(document).ready(function(){
	$('#cpf').mask('000.000.000-00', {reverse: true});
	$('#cep').mask('00000-000');
	$("#dia").mask('00');
	$("#mes").mask('00');
	$("#ano").mask('0000');
	$("#ddd").mask('00');
	$("#celular").mask('0000-0000');
});