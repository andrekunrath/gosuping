/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;   
    if((tecla>47 && tecla<58)) return true;
    else{
    	if (tecla==8 || tecla==0) return true;
	else  return false;
    }
}


$('.box-select-item .item').click(function(e){
    var classSelected = $(this).hasClass('selected');
    if(classSelected)
        return false;
    
    var classUnavailable = $(this).hasClass('unavailable');
    if(classUnavailable)
        return false;
    
    var classAvailable = $(this).hasClass('available');
    if(classAvailable)
    {
        var itens = $('.box-select-item .item');
        itens.removeClass('selected');
        itens.find('input').each(function(){
            $(this).attr('selecionado', false).prop("checked", false);
        });
        $(this).addClass('selected');
        $(this).find('input').attr('selecionado', true).prop("checked", true);
    }
});

$("#frmConfirmar").submit(function(){

    if (!$("input[type=radio]").is(":checked"))
    {
        return false;
    }
});